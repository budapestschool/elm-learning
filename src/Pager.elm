module Pager exposing (..)

import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)


type alias Page =
    String


type alias PagerModel =
    { currentPage : Int, pages : List Page }


initialPagerModel : Int -> PagerModel
initialPagerModel startPage =
    { currentPage = startPage, pages = [ "ablak", "baba", "zsiráf" ] }


type PageMsg
    = PageNext
    | PagePrev


updatePager : PageMsg -> PagerModel -> PagerModel
updatePager msg model =
    let
        totalPages =
            List.length model.pages
    in
    case msg of
        PageNext ->
            if model.currentPage == totalPages - 1 then
                { model | currentPage = 0 }

            else
                { model | currentPage = model.currentPage + 1 }

        PagePrev ->
            if model.currentPage == 0 then
                { model | currentPage = totalPages - 1 }

            else
                { model | currentPage = model.currentPage - 1 }


getElementAt : Int -> List a -> Maybe a
getElementAt ix l =
    List.head (List.drop ix l)


viewPager : PagerModel -> Html PageMsg
viewPager model =
    div []
        [ button [ onClick PagePrev ] [ text "<<" ]
        , button [ onClick PageNext ] [ text ">>" ]
        , text (getElementAt model.currentPage model.pages |> Maybe.withDefault "nincs")
        ]

