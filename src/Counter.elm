module Counter exposing (CounterModel, CounterMsg, initialCounterModel, updateCounter, viewCounter)

import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)


type alias CounterModel =
    { count : Int }


initialCounterModel : () -> CounterModel
initialCounterModel _ =
    { count = 0 }


type CounterMsg
    = Increment
    | Decrement


updateCounter : CounterMsg -> CounterModel -> CounterModel
updateCounter msg model =
    case msg of
        Increment ->
            { model | count = model.count + 1 }

        Decrement ->
            { model | count = model.count - 1 }


viewCounter : CounterModel -> Html CounterMsg
viewCounter model =
    div []
        [ button [ onClick Increment ] [ text "+1" ]
        , div [] [ text <| String.fromInt model.count ]
        , button [ onClick Decrement ] [ text "-1" ]
        ]

