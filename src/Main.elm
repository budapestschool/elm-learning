module Main exposing (main)

import Browser
import Counter exposing (CounterModel, CounterMsg, initialCounterModel, updateCounter, viewCounter)
import Html exposing (Html, div, input, label, q, text, wbr)
import Html.Attributes exposing (checked, for, id, name, type_)
import Html.Events exposing (onClick)
import Pager exposing (PageMsg, PagerModel, initialPagerModel, updatePager, viewPager)
import Url exposing (Url)
import Url.Parser exposing ((</>), int, map, oneOf, s)


type alias EmbeddedComponent param msg a =
    { init : param -> a
    , update : msg -> a -> a
    , view : a -> Html msg
    }


type ActiveComponent
    = ShowCounter
    | ShowPager



{- counter app -}


counter : EmbeddedComponent () CounterMsg CounterModel
counter =
    { init = initialCounterModel
    , update = updateCounter
    , view = viewCounter
    }



{- pager app -}


pager : EmbeddedComponent Int PageMsg PagerModel
pager =
    { init = initialPagerModel
    , update = updatePager
    , view = viewPager
    }



{- main app -}


type alias Model =
    { counterModel : CounterModel
    , pagerModel : PagerModel
    , activeComponent : ActiveComponent
    }


type Route
    = Counter
    | Pager Int


urlParser : Url.Parser.Parser (Route -> a) a
urlParser =
    oneOf
        [ map Counter (s "counter")
        , map Pager (s "pager" </> int)
        ]


initialModel : Url -> Model
initialModel url =
    let
        route =
            Url.Parser.parse urlParser url

        initPagerValue =
            case route of
                Just (Pager x) ->
                    x

                _ ->
                    0

        -- Url.Parser.parse Url.Parser.int u:rl
    in
    { counterModel = initialCounterModel ()
    , pagerModel = initialPagerModel initPagerValue
    , activeComponent =
        case route of
            Just Counter ->
                ShowCounter

            Just (Pager _) ->
                ShowPager

            Nothing ->
                ShowPager
    }


type Msg
    = PageMsgType PageMsg
    | CounterMsgType CounterMsg
    | SwitchComponent ActiveComponent
    | NoOp


update : Msg -> Model -> Model
update msg model =
    case msg of
        PageMsgType pageMsg ->
            { model | pagerModel = pager.update pageMsg model.pagerModel }

        CounterMsgType counterMsg ->
            { model | counterModel = counter.update counterMsg model.counterModel }

        SwitchComponent activeComponent ->
            { model | activeComponent = activeComponent }

        NoOp ->
            model



{- view : Model -> Browser.Document Msg -}


view : Model -> Browser.Document Msg
view model =
    { title = "ez a title"
    , body =
        [ div []
            [ div []
                [ input [ type_ "radio", name "component", id "showCounter", checked (model.activeComponent == ShowCounter), onClick (SwitchComponent ShowCounter) ] []
                , label [ for "showCounter" ] [ text "Counter" ]
                , input [ type_ "radio", name "component", id "showPager", checked (model.activeComponent == ShowPager), onClick (SwitchComponent ShowPager) ] []
                , label [ for "showPager" ] [ text "Pager" ]
                ]
            , case model.activeComponent of
                ShowCounter ->
                    Html.map CounterMsgType (viewCounter model.counterModel)

                ShowPager ->
                    Html.map PageMsgType (viewPager model.pagerModel)
            ]
        ]
    }


main : Program () Model Msg
main =
    Browser.application
        { init = \flags url key -> ( initialModel url, Cmd.none )
        , view = view
        , update = \msg model -> ( update msg model, Cmd.none )
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = \_ -> NoOp
        , onUrlChange = \_ -> NoOp
        }



{-
   application :
       { init : flags -> Url.Url -> Navigation.Key -> ( model, Cmd msg )
       , view : model -> Document msg
       , update : msg -> model -> ( model, Cmd msg )
       , subscriptions : model -> Sub msg
       , onUrlRequest : UrlRequest -> msg
       , onUrlChange : Url.Url -> msg
       }
-}
